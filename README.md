# POSTGIS

This repository contains the source code for the docker image `ideaplexus/postgis`.

These docker images extending the original `PostgreSQL` images with  `PostGIS`, `pgRouting` and `Osm2pgsql`.

## Usage

Since this image is created on top of the PostgreSQL docker image, please refer to the original [documentation](https://github.com/docker-library/docs/blob/master/postgres/README.md).
Note that the original entrypoint file is overwritten by `initdb-postgis.sh`. Also there is the `update-postgis.sh`, which can be used to update an existing PostgreSQL database with the latest extension versions. 