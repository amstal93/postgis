ARG POSTGRES_VERSION=latest
FROM postgres:${POSTGRES_VERSION}-alpine

# postgis dependency
ENV GEOS_VERSION=3.9.1
ENV GEOS_SHA256=e9e20e83572645ac2af0af523b40a404627ce74b3ec99727754391cdf5b23645

# postgis dependency
ENV GDAL_VERSION=3.2.2
ENV GDAL_SHA256=1e9554870ee7d7f273e849f7b86164f0e6aadc476a880cb2c2bd6885d024c678

# postgis dependency
ENV PROJ4_VERSION=6.3.2
ENV PROJ4_SHA256=940c2260092129134495dac4999dfbc3647be50e31274e3dbd5ca66e4a40f461

# postgis dependency
ENV PROTOBUFC_VERSION=1.3.3
ENV PROTOBUFC_SHA256=8515da8b84ea36768ae69a52f9ede8d718936d6058077265b6e458cfc55f8bdb

ENV POSTGIS_VERSION=3.1.1
ENV POSTGIS_SHA256=28e9cb33d5a762ad2aa72513a05183bf45416ba7de2316ff3ad0da60c4ce56e3

ENV PGROUTING_VERSION=3.1.3
ENV PGROUTING_SHA256=54b58e8c4ac997d130e894f6311a28238258b224bb824b83f5bfa0fb4ee79c60

ENV OSM2PGSQL_VERSION=1.4.2
ENV OSM2PGSQL_SHA256=fc68283930ccd468ed9b28685150741b16083fec86800a4b011884ae22eb061c

RUN set -eux \
  \
  && apk update \
  # workaround for alpine/kaniko bug, @see https://github.com/GoogleContainerTools/kaniko/issues/1297
  && apk upgrade --no-cache --ignore alpine-baselayout \
  && apk add --no-cache --virtual .fetch-deps \
    ca-certificates \
    openssl \
    tar \
  \
  && wget -q -O geos.tar.gz "https://github.com/libgeos/geos/archive/$GEOS_VERSION.tar.gz" \
  && sha256sum geos.tar.gz \
  && echo "$GEOS_SHA256 *geos.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/geos \
  && tar \
    --extract \
    --file geos.tar.gz \
    --directory /usr/src/geos \
    --strip-components 1 \
  && rm geos.tar.gz
RUN set -eux \
  \
  && wget -q -O gdal.tar.gz "https://github.com/OSGeo/gdal/archive/v$GDAL_VERSION.tar.gz" \
  && sha256sum gdal.tar.gz \
  && echo "$GDAL_SHA256 *gdal.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/gdal \
  && tar \
    --extract \
    --file gdal.tar.gz \
    --directory /usr/src/gdal \
    --strip-components 1 \
  && rm gdal.tar.gz \
  \
  && wget -q -O proj4.tar.gz "https://github.com/OSGeo/proj.4/archive/$PROJ4_VERSION.tar.gz" \
  && sha256sum proj4.tar.gz \
  && echo "$PROJ4_SHA256 *proj4.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/proj4 \
  && tar \
    --extract \
    --file proj4.tar.gz \
    --directory /usr/src/proj4 \
    --strip-components 1 \
  && rm proj4.tar.gz \
  \
  && wget -q -O protobufc.tar.gz "https://github.com/protobuf-c/protobuf-c/archive/v$PROTOBUFC_VERSION.tar.gz" \
  && sha256sum protobufc.tar.gz \
  && echo "$PROTOBUFC_SHA256 *protobufc.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/protobufc \
  && tar \
    --extract \
    --file protobufc.tar.gz \
    --directory /usr/src/protobufc \
    --strip-components 1 \
  && rm protobufc.tar.gz \
  \
  && wget -q -O postgis.tar.gz "https://github.com/postgis/postgis/archive/$POSTGIS_VERSION.tar.gz" \
  && sha256sum postgis.tar.gz \
  && echo "$POSTGIS_SHA256 *postgis.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/postgis \
  && tar \
      --extract \
      --file postgis.tar.gz \
      --directory /usr/src/postgis \
      --strip-components 1 \
  && rm postgis.tar.gz \
  \
  && wget -q -O pgrouting.tar.gz "https://github.com/pgRouting/pgrouting/archive/v$PGROUTING_VERSION.tar.gz" \
  && sha256sum pgrouting.tar.gz \
  && mkdir -p /usr/src/pgrouting \
  && tar \
      --extract \
      --file pgrouting.tar.gz \
      --directory /usr/src/pgrouting \
      --strip-components 1 \
  && rm pgrouting.tar.gz \
  \
  && wget -q -O osm2pgsql.tar.gz "https://github.com/openstreetmap/osm2pgsql/archive/$OSM2PGSQL_VERSION.tar.gz" \
  && sha256sum osm2pgsql.tar.gz \
  && echo "$OSM2PGSQL_SHA256 *osm2pgsql.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/osm2pgsql \
  && tar \
      --extract \
      --file osm2pgsql.tar.gz \
      --directory /usr/src/osm2pgsql \
      --strip-components 1 \
  && rm osm2pgsql.tar.gz \
  \
  && apk add --no-cache --virtual .build-deps \
    build-base \
    cmake \
    clang \
    llvm10 \
    linux-headers \
    expat-dev \
    zlib-dev \
    bzip2-dev \
    lua5.2-dev \
    boost-dev \
    boost-filesystem \
    boost-system \
    boost-thread \
    protobuf-dev \
    autoconf \
    automake \
    libtool \
    libxml2-dev \
    json-c-dev \
    pcre-dev \
    sqlite-dev \
    sqlite \
  && cd /usr/src/geos \
  && ./autogen.sh \
  && ./configure \
  && make -j "$(nproc)" \
  && make install \
  && cd /usr/src/proj4 \
  && ./autogen.sh \
  && ./configure \
    -- prefix=/usr/local \
  && make -j "$(nproc)" \
  && make install \
  && cd /usr/src/gdal/gdal \
  && ./configure \
  && make -j "$(nproc)" \
  && make install \
  && cd /usr/src/protobufc \
  && ./autogen.sh \
  && ./configure \
  && make -j "$(nproc)" \
  && make install \
  && cd /usr/src/postgis \
  && ./autogen.sh \
  && ./configure \
  && make -j "$(nproc)" \
  && make install \
  && cd /usr/src/pgrouting \
  && mkdir build \
  && cd build \
  && cmake .. \
  && make -j "$(nproc)" \
  && make install \
  # fix lua detection
  && ln -s /usr/lib/lua5.2/liblua.so /usr/lib/liblua.so \
  && cd /usr/src/osm2pgsql \
  && mkdir build \
  && cd build \
  && cmake .. \
  && make -j "$(nproc)" \
  && make install \
  && rm -rf \
    /usr/local/lib/cmake \
    /usr/local/lib/pkgconfig \
    /usr/src/* \
  && apk del .fetch-deps .build-deps \
  && RUN_DEPS="$( \
      scanelf --needed --nobanner --format '%n#p' /usr/local/bin/* /usr/local/lib/* /usr/local/lib/postgresql/* \
          | tr ',' '\n' \
          | sort -u \
          | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
  )" \
  && apk add --no-cache --virtual .run-deps \
    $RUN_DEPS

COPY initdb-postgis.sh /docker-entrypoint-initdb.d/postgis.sh
COPY update-postgis.sh /usr/local/bin
